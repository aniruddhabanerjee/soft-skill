## Introduction to Firewall

A firewall is a piece of software or hardware program that can monitor and control network traffic, both incoming and outgoing. It establishes, a kind of barrier between reliable internal and unknown external networks. A Software firewall is install in our computers. A Hardware Firewall is a device that is placed in between your network and untrusted internet.

If more than one computer is connected to a network then it is necessary to protect your network from untrusted internet via a hardware firewall but it is also necessary to protect this computer from software firewall so that if one computer get infected from virus the other will remain free from the virues.

**Accept** : Allow the traffic  <br>
**Reject** : Block the traffic but reply with an “unreachable error”<br>
**Drop** : Block the traffic with no reply<br>

## Generation of Firewall

Firewalls can be categorized based on its generation.
### Packet filters

A first-generation network firewall used to monitor packets, in other words, bytes transferring between computers. They are still used today, but modern firewalls have traversed a long path with technological development.

### Stateful filters

Second-generation firewalls that came around 1990 that perform the same work as packet filters do besides monitoring activities between two endpoints. Stateful filters are vulnerable to DDoS attacks.

### Application layer

The third generation firewall that could understand protocols and applications like FTP, HTTP. Hence, it could detect unwanted applications trying to bypass the network firewall.

### Next-generation firewall (NGFW)

It is a deeper or advanced inspection of the application layer, which includes intrusion prevention systems (IPS), web application firewall (WAF), and user identity management.

## Limitations of Firewalls 

*   Firewalls cannot protect against what has been authorized.
*   It cannot stop an unauthorized user intentionally using their access for unwanted purposes.
*   Firewalls cannot fix poor administrative practices or poorly designed security policies.
*   It cannot stop attacks if the traffic does not pass through them.
*   They are only as effective as the rules they are configured to enforce.

References:<br>
https://www.youtube.com/watch?v=eO6QKDL3p1I&list=PLBbU9-SUUCwV7Dpk7GI8QDLu3w54TNAA6<br>
https://www.geeksforgeeks.org/introduction-of-firewall-in-computer-network/
